# Dokumente

**Deprecated: The functionality is not integrated in the [Mathezirkel App](https://gitlab.com/mathezirkel/mathezirkel-app/exports).**

## Beschreibung

Das Repository enthält LaTeX-Dokumente für die Organisation von Veranstaltungen des Mathezirkels.

## Abhängigkeiten
* LaTeX Distribution
* Python (inkl. package yaml)
* jq

## Anleitung

## Dokumentation

### Konfigurationsdatei anpassen

Passe die Konfigurationsdatei und event.sty entsprechend der gewünschten Outputs an.

Ein Beispiel ist im Ordner [example](./example/) zu finden.

### Skript ausführen

```commandline
$ export MC_LIST_DIR=path/to/lists
$ export MC_DOCUMENT_DIR=path/to/documents
$ export MC_CONFIG_FILE=path/to/config/file
$ export MC_IMAGE_DIR=path/to/config/image
$ export MC_CONFIG_DIR=path/to/latex/stylefile
$ export MC_RAUMPLAN_DIR=path/to/roomplan
$ /bin/sh ./run.sh
```

### Umgebungsvariablen

* MC_LIST_DIR: Pfad des Ordners, in dem sich die csv Dateien befinden bzw. exportiert werden.
* MC_DOCUMENT_DIR: Pfad des Ordners, in die tex Dateien exportiert werden.
* MC_CONFIG_FILE: Pfad der Konfigurationsdatei
* MC_IMAGE_DIR: Pfad zu den Images des Mathezirkels
* MC_CONFIG_DIR: Pfad des Ordners, in dem die Datei [event.sty](./example/event.sty) liegt
* MC_RAUMPLAN_DIR: Pfad des Ordners, in dem die Raumpläne liegen

### Konfigurationsdatei

* documents: Liste an Dateien relativ zu [src](./src/) die kompiliert werden sollen

## License
This project is licenced unter GPL-3.0, see [LICENSE](./LICENSE).
