#!/bin/sh

# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


MC_CLS_DIR=$(realpath ./cls/cls/)

export TEXINPUTS=$MC_CLS_DIR:$MC_IMAGE_DIR//:$MC_LIST_DIR//:$MC_DOCUMENT_DIR//:$MC_CONFIG_DIR:$MC_RAUMPLAN_DIR//:$TEXINPUTS

cd src

tex_files=$(python3 -c "import sys, yaml, json; json.dump(yaml.safe_load(sys.stdin), sys.stdout, indent=2)" < $MC_CONFIG_FILE | jq -r '.documents[]')
for file in $tex_files; do
    latexmk -cd -output-directory=$(dirname "${MC_DOCUMENT_DIR}/$file") $file;
done

cd ..
